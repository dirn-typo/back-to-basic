## Hybrid Back To Basic is a Conky theme that use minimal Lua script.

| Theme | Distro Logos |
| ----------- | ----------- |
| ![Conky Image](/screenshots/screenshot.png) | ![Conky Image](/screenshots/distro-logos.png) |


## Change Log

- v1.0.1
    - updated manjaro icons
    - fixed lua script to make it works with conky-lua-nv 1.21.2-1

- v1.0
    - initial version


## Dependencies
- **Arch based distro package**
    - conky-lua-nv (with nvidia object) **OR** conky-lua
 
- **Debian based distro package**
    - conky-all
 
- **Fonts**
    - NotoSans (default)


## Installation
- Fonts
    - Install fonts like normal.

- Download the project and extract it.
    - Inside the project search for install.sh file
    - Set file install.sh to be executable
    - Execute the script in Konsole / Terminal

            $ ./install.sh


## To use
- Open Konsole / Terminal and run
            $ conky -c ~/.config/conky/back-to-basic/back-to-basic.conf &


- Alternatively
    - Create symlink from your config
    - Paste it to home folder
    - Rename symlink to .conkyrc
    - In terminal run conky

            $ conky &


## Auto Start
- Inside startup folder search for back-to-basic-startup.sh file
- Set back-to-basic.sh to be executable
- Change the home path to your home path, seems like I can't use ~ at startup script
- Add back-to-basic-startup.sh to auto start program
- You can adjust sleep time in the script accordingly


## Customizations
- **Line sketches toggle**
    - Line sketches background can be turn on or off from conky config file. This is useful since we try to keep 2 columns into one single script. Need to adjust voffset value when we made changes to elements (number of CPUs, cores etc):

            -- to turn on
            lua_draw_hook_pre = 'main true'

            -- to turn off
            lua_draw_hook_pre = 'main false'

 
- **Distro Logo**
    - Distro logo is render from Lua script instead of Conky, this is due to Conky image object is causing the image to become transparent. To change open back-to-basic.lua file and search for draw_logo() function, don't forget to change home_dir value. Edit the line below:

            local imagefile = home_dir .. "/.config/conky/back-to-basic/images/distro-1a.png"

    - **Logos available**
        1. Manjaro
        2. Linux Mint
        3. Arch Linux
        4. Ubuntu
        5. MX Linux
        6. Debian
        7. Elementary OS
        8. Pop! OS
        9. Solus
        10. Fedora
        11. Zorin
        12. PCLinuxOS
        13. Kali Linux
 

- **CPUs usage and cores temperature**
    - To adjust CPUs, add or remove the code like example below

            ${color2}${font NotoSans:bold:size=9}CPU USAGE ${color}
            ${font NotoSans:style=Bold:size=9}CPU 01 - ${font NotoSans:size=9}${cpu cpu1}% ${goto 90}\
            ${if_match ${cpu cpu1} <= 75}${color2}${else}${if_match ${cpu cpu1} > 90}${color4}${else}${if_match ${cpu cpu1} > 75}${color3}${endif}${endif}${endif} \
            ${cpubar cpu1 4, 150}${color}
    
    - As for cores, add or remove the code example below.

            ${font NotoSans:style=Bold:size=9}CORE 01 - ${font NotoSans:size=9}\
            ${if_match ${platform coretemp.0/hwmon/hwmon5 temp 2} <= 75}${color}${else}${if_match ${platform coretemp.0/hwmon/hwmon5 temp 2} > 90}${color4}${else}\
            ${if_match ${platform coretemp.0/hwmon/hwmon5 temp 2} > 75}${color3}${endif}${endif}${endif}\
            ${platform coretemp.0/hwmon/hwmon5 temp 2}°C ${color}

        Need to check hwmon on your system from the path below and made the change to Conky accordingly:

            /sys/bus/platform/devices/coretemp.0/hwmon/

- **Network interface** 
    - In back-to-basic.config, you need to change network card interface. Replace enp7s0f1 and wlp0s20f3 with yours. If you only have a single interface you can commented out the second network interface. However, this is optional.

            #${if_existing /proc/net/route wlp0s20f3}\
            #${goto 250}${font Play:style=Bold:size=8}Internal IP - wlp0s20f3 ${font Play:size=8}$# {alignr}${addr wlp0s20f3}
            #${goto 250}    ${font Play:style=Bold:size=8}Download
            #${goto 250}        ${color2}${font Play:size=8}Speed ${alignr} ${downspeed # wlp0s20f3}
            #${goto 250}        ${color}Total ${alignr} ${totaldown wlp0s20f3}
            #${goto 250}    ${font Play:style=Bold:size=8}Upload
            #${goto 250}        ${color2}${font Play:size=8}Speed ${alignr} ${upspeed # wlp0s20f3}
            #${goto 250}        ${color}Total ${alignr} ${totalup wlp0s20f3}
            #${endif}\

    - To check you network interface use command:

            $ ip link show


## Disclaimer
I am not the original creator of the conkyrc file (has been tweaked for my usage).
It was downloaded way back in 2009 and I've no information of the original creator. Credit should go to him/her.

I do not own any of the distro logos bundled with this script. Please inform me if in case any of the logo are not allowed to be share.
I will remove it as soon as possible.


## External Links
[Arch Manual Pages](https://jlk.fjfi.cvut.cz/arch/manpages/man/conky.1)

[Conky Objects](http://conky.sourceforge.net/variables.html)

[Lua 5.4 Reference Manual](https://www.lua.org/manual/5.4/)

[Cairo Samples](https://cairographics.org/samples/)