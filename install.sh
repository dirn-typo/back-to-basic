#!/bin/bash

if [ -d ~/.config/conky/back-to-basic/ ]
then
    rm -R ~/.config/conky/back-to-basic/
    echo 'back-to-basic uninstalled'
fi

rsync -IrW --stats --exclude={'.git','deploy.sh','install.sh','readme.md','fonts','screenshots','bad-experiment'} $(pwd) ~/.config/conky/
echo 'back-to-basic installed'