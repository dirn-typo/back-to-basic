#!/bin/bash

if [ -d ~/source-code/deploy/back-to-basic/ ]
then
    rm -R ~/source-code/deploy/back-to-basic/
    echo 'package deleted'
fi

rsync -IrW --stats --exclude={'.git','deploy.sh','readme.md','screenshots','bad-experiment'} $(pwd) ~/source-code/deploy/
echo 'back-to-basic copied'

cd ~/source-code/deploy
echo 'go to deploy folder'

tar -czvf back-to-basic.tar.gz back-to-basic
echo 'package archived'